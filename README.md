
## MSSV: 2180607587
## Lớp: 21DTHD4
## Khoa: Công Nghệ Thông Tin

| Title                |  Teachers delete students based on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a teacher, I want to remove student information from the system based on ID to search.|
| Acceptance Criterion | Acceptence Criterion 1:Students can be deleted from the system when ID is found<br>Acceptence Criterion 2: Display a notification if the student has been successfully deleted from the system  |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Luong Thien Hung                            |
![Semantic description of image](387537968_1529999864403502_4538188284849181568_n.png) 



## Họ tên: Nguyễn Nhân Quỳnh Như

## MSSV: 2180608719

##
## Lớp: 21DTHD4
| Title               | Teacher changes students's score                    |
|:-------------------:|:-----------------------------------------------|
| Value Statement     | As a teacher, I want to change student's score | 
| Acceptence Criter   | Acceptence Criterion: If student doesn't take the exam, they get 0 points;<br>if their score is wrong then correct it.|
| Definition Of Done  | - Unit Tests Passed<br>- Acceptence Criteria Met<br>- Code Reviewed<br>- Functional Tests Passed<br>- Non-Functional Requirements Met<br>- Product Owner Accepts User Story|
| Owner               | Nguyen Nhan Quynh Nhu                          | 
##
![Semantic description of image](z4800948312725_7e31b157121f6bd6af2d65cdda589c16.jpg)

##
## Họ tên: Huynh My Nhat Mai

## MSSV: 2180607724

## Lớp: 21DTHD4

| Title                | Search students's information base on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a Manager,<br>I want to find student easily.|
| Acceptance Criterion | Acceptance Criterion 1:<br>Check if that student exist<br>Find students to checkout their score |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Huynh My Nhat Mai                           | 


## 2180607411_DangVanDat
## Đặng Văn Đạt
## 2180607411
## 21DTHD4

| Title                | Teacher insert student to the list             |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | If there is new,as a teacher,I want to add them in list.|
| Acceptance Criterion | Acceptance Criterion : Add new student information to the list|
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Dang Van Dat  |
![Semantic description of image](z4800994504375_e02a60b4677a6469a5c0db2567de1845.jpg) 




## Họ Tên:Nguyễn Hoàng Chương
## MSSV: 2180607334
## Khoa: Công Nghệ Thông Tin
| Title:                | Manager arrange Student with increasing avgScore                                                                                               |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement:      | As a Manager,<br>I want to arrange student with increasing avgScore<br>so I can easily find the student with the highest avgScore.             |
| Acceptance Criterion: | Acceptance Criterion 1:<br>When managing chooses to select the sorting option,<br>the student avgScore will be sorted ascending or descending. |
| Definition of Done:   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story                                                                                    |
| Owner               | Nguyen Hoang Chuong                          | 
##
![Semantic description of image](https://i.imgur.com/J3cwoVt.png)
